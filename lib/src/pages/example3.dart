import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class example3 extends StatelessWidget {
  example3 ({Key? key}) : super(key: key);

  final titles = ['Courtois', 'Militão', 'Rüdiger', 'Alaba',
    'carvajal', 'Kroos', 'modrić', 'Casemiro', 'Valverde','benzema','Vinícius Júnior'];

  final subtitle = ['goalkeeper', 'defense ', 'defense', 'defense',
  'defense', 'midfielder', 'midfielder', 'midfielder', 'midfielder','forward','forward'];

  final Image =[
    NetworkImage('https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltbafd1939358ef409/61e47278057fc639e284699c/GettyImages-1237772875.jpg'),
    NetworkImage('https://images.thaiza.com/content/b/440001.jpg'),
    NetworkImage('https://pbs.twimg.com/media/FSZyR57X0AA6vKg?format=jpg&name=900x900'),
    NetworkImage('https://icdn.football-espana.net/wp-content/uploads/2022/05/David-Alaba-1-1020x680.jpeg'),
    NetworkImage('https://cdn.resfu.com/media/img_news/goal_danicarvajal-cropped_uwzdb72e9365137gl26raedq5.jpg'),
    NetworkImage('https://cdn.vox-cdn.com/thumbor/Kl7jNxjIlZHoXjAS8APYScl3ots=/0x0:5184x3456/1200x800/filters:focal(3106x329:3934x1157)/cdn.vox-cdn.com/uploads/chorus_image/image/70427030/1237952516.0.jpg'),
    NetworkImage('https://pbs.twimg.com/media/FNcO0GbWQBE_oz0.jpg'),
    NetworkImage('https://cdn.resfu.com/media/img_news/afp_fr_022d20da6fbb3be56d536b4c1bc76f753e68ed50.jpg?size=1200x&lossy=1'),
    NetworkImage('https://pbs.twimg.com/media/E9jfMofXIAErsvI.jpg:large'),
    NetworkImage('https://www.dmarge.com/wp-content/uploads/2022/03/benzema.jpg'),
    NetworkImage('https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/blt05466b3c8cf7cd2d/61a3fc32dd20186d161a6d98/Vinicius_Junior_(2).jpg'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Real Madrid'),

      ),
      body:  ListView.builder(
        itemCount: titles.length,
          itemBuilder: (context,index){
            return Column(
              children: [
                ListTile(
                  leading: CircleAvatar(backgroundImage: Image[index],),
                  title: Text('${titles[index]}', style: TextStyle(fontSize: 18),),
                  subtitle: Text('${subtitle[index]}',style: TextStyle(fontSize: 15),),
                  trailing: Icon(Icons.add_alert_sharp,size: 25,),
                ),
                Divider(thickness: 1,),
              ],
            );
    },

      ),
    );
  }
}
