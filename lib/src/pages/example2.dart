

import 'package:flutter/material.dart';

class  example2 extends StatelessWidget {
  const example2 ({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text('list view2'),
      ),
      body: ListView(
        children: [
          ListTile(
            leading: Icon(
              Icons.delivery_dining ,
              size: 25,
            ),
            title: Text(
              '8.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
              'Hello woed xxx.'
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.departure_board,
              size: 25,
            ),
            title: Text(
              '9.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
                'Hello woed xxx.'
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.delivery_dining ,
              size: 25,
            ),
            title: Text(
              '10.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
                'Hello woed xxx.'
            ),
            trailing: Icon(
              Icons.notifications_none,
              size: 25,),
            onTap: (){
              print("Train");
            },
          ),
          ListTile(
            leading: Icon(
              Icons.accessible_forward_rounded,
              size: 25,
            ),
            title: Text(
              '11.00 A.M.',
              style: TextStyle(fontSize: 18),
            ),
            subtitle: Text(
                'Hello woed xxx.'
            ),
            trailing: Icon(
              Icons.add_a_photo_outlined,
              size: 25,),
            onTap: (){
              print("Ckik");
            },
          ),
        ],
      ),

    );
  }
}
